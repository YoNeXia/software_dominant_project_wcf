﻿using System.ServiceModel;
using WCFMiddleware.Model;

/// <summary>
/// Expose les méthodes du service.
/// </summary>
[ServiceContract]
public interface IService
{
    [OperationContract]
    Message Dispatch(Message message);
}
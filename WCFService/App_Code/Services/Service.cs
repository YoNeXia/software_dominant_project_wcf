﻿using System;
using System.Configuration;
using System.Linq;
using WCFMiddleware.BU;
using WCFMiddleware.Interfaces;
using WCFMiddleware.Model;
using WCFMiddleware.Utils;

/// <summary>
/// Classe de service.
/// </summary>
public class Service : IService
{
    private IAuthentication authInstance = new Authentication();
    private IDecryptor decryptInstance = new Decryptor();
    private Utils utils = new Utils();

    /// <summary>
    /// Envoie le message puis le retourne.
    /// </summary>
    /// <param name="message">Message à envoyer.</param>
    /// <returns>Message de réponse.</returns>
    public Message Dispatch(Message message)
    {
        if ((message.Application.Token == ConfigurationManager.AppSettings.Get("tokenClient") || message.Application.Token == ConfigurationManager.AppSettings.Get("tokenJee")) && message.User != null)
        {
            switch (message.Operation.Name)
            {
                case "Authenticate":
                    message.User.Token = this.authInstance.Authenticate(message.User);
                    break;

                case "Decrypt":
                    this.decryptInstance.DecryptFiles(message);
                    break;

                case "SendSecretFile":
                    utils.SendMail(this.authInstance.GetLoginByToken(message.User.Token), message.Files.FirstOrDefault());
                    break;

                default:
                    throw new Exception("Operation name not found");
            }
            message.Operation.Infos = "Operation success";
            message.Operation.Statut = true;
        }
        else
        {
            message.Operation.Infos = "Bad token";
            message.Operation.Statut = false;
        }
        message.Application.Token = ConfigurationManager.AppSettings.Get("tokenApp");
        return message;
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WCFMiddleware.Model;

namespace WCFDAO.DAO.Tests
{
    [TestClass()]
    public class UserDAOTests
    {
        private static User user1 = new User()
        {
            Login = "test.test@test.fr",
            Password = "passTest",
            Token = "test123"
        };

        private static User user2 = new User()
        {
            Login = "test2.test2@test2.fr",
            Password = "passTest2",
            Token = "test321"
        };

        private List<User> users = new List<User>()
        {
            user1,
            user2
        };

        [TestMethod()]
        public void GetUserTokenTest()
        {
            Assert.AreEqual(user1.Token, users.Find(u => u.Login == "test.test@test.fr" && u.Password == "passTest").Token);
            Assert.AreNotEqual(user1.Token, null);
            Assert.IsFalse(users.Exists(u => u.Login == "test.test@test.fr" && u.Password == "toto"));
            Assert.IsTrue(users.Exists(u => u.Login == "test2.test2@test2.fr" && u.Password == "passTest2"));
        }

        [TestMethod()]
        public void GetUserLoginByTokenTest()
        {
            Assert.AreEqual(user1.Login, users.Find(u => u.Token == "test123").Login);
            Assert.IsFalse(users.Exists(u => u.Token == "123456789"));
        }
    }
}
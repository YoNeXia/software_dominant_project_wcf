﻿using System.Runtime.Serialization;

namespace WCFMiddleware.Model
{
    /// <summary>
    /// Getters et Setters du Fichier
    /// <list type="bullet">
    /// <item>
    /// <term>Title</term>
    /// <description>Titre du fichier.</description>
    /// </item>
    /// <item>
    /// <term>ContentString</term>
    /// <description>Contenu du fichier.</description>
    /// </item>
    /// <item>
    /// <term>Key</term>
    /// <description>Clé du fichier.</description>
    /// </item>
    /// <item>
    /// <term>SecreteInformation</term>
    /// <description>Information secrète dans le fichier.</description>
    /// </item>
    /// </list>
    /// </summary>
    [DataContract]
    public class File
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string ContentString { get; set; }

        [DataMember]
        public string Key { get; set; } = null;

        [DataMember]
        public string SecreteInformation { get; set; } = null;
    }
}
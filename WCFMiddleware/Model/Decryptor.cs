﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCFMiddleware.Model
{
    class Decryptor
    {
        private static string FindKey(string fileContent)
        {
            //Brute force : assume une clé de 4 caractères de l'alphabet français en minuscule.
            StringBuilder Mdp = new StringBuilder(new String('a', 4));
            while (true)
            {
                String value = Mdp.ToString();

                //Sortie mot de passe

                if (value.All(item => item == 'z'))
                    break;

                for (int i = 3; i >= 0; --i)
                    if (Mdp[i] != 'z')
                    {
                        Mdp[i] = (Char)(Mdp[i] + 1);
                        break;
                    }
                    else
                        Mdp[i] = 'a';
            }
            return Mdp.ToString();
        }
    }
}

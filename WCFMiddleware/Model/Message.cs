﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCFMiddleware.Model
{
    /// <summary>
    /// Getters et Setters du Message.
    /// <list type="bullet">
    /// <item>
    /// <term>Operation</term>
    /// <description>Type d'opération de message.</description>
    /// </item>
    /// <item>
    /// <term>Application</term>
    /// <description>Application.</description>
    /// </item>
    /// <item>
    /// <term>User</term>
    /// <description>Utilisateur.</description>
    /// </item>
    /// <item>
    /// <term>Files</term>
    /// <description>Liste de fichiers.</description>
    /// </item>
    /// </list>
    /// </summary>
    [DataContract]
    public class Message
    {
        [DataMember]
        public Operation Operation { get; set; }

        [DataMember]
        public Application Application { get; set; }

        [DataMember]
        public User User { get; set; }

        [DataMember]
        public List<File> Files { get; set; }
    }
}
﻿using System.Runtime.Serialization;

namespace WCFMiddleware.Model
{
    /// <summary>
    /// Getters et Setters de l'Utilisateur.
    /// <list type="bullet">
    /// <item>
    /// <term>Login</term>
    /// <description>Adresse mail de connexion.</description>
    /// </item>
    /// <item>
    /// <term>Password</term>
    /// <description>Mot de passe.</description>
    /// </item>
    /// <item>
    /// <term>Token</term>
    /// <description>Jeton Utilisateur.</description>
    /// </item>
    /// </list>
    /// </summary>
    [DataContract]
    public class User
    {
        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Token { get; set; }
    }
}
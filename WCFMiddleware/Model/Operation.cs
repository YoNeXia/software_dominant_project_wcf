﻿using System.Runtime.Serialization;

namespace WCFMiddleware.Model
{
    /// <summary>
    /// Getters et Setters de l'Operation.
    /// <list type="bullet">
    /// <item>
    /// <term>Status</term>
    /// <description>Statut de l'opération.</description>
    /// </item>
    /// <item>
    /// <term>Name</term>
    /// <description>Nom de l'opération</description>
    /// </item>
    /// <item>
    /// <term>Infos</term>
    /// <description>Informations sur l'opération.</description>
    /// </item>
    /// <item>
    /// <term>Version</term>
    /// <description>Version de l'Application.</description>
    /// </item>
    /// </list>
    /// </summary>
    [DataContract]
    public class Operation
    {
        [DataMember]
        public bool Statut { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Infos { get; set; }

        [DataMember]
        public string Version { get; set; }
    }
}
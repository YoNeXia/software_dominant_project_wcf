﻿using System.Runtime.Serialization;

namespace WCFMiddleware.Model
{
    /// <summary>
    /// Getters et Setters de l'application.
    /// <list type="bullet">
    /// <item>
    /// <term>Version</term>
    /// <description>Version de l'application.</description>
    /// </item>
    /// <item>
    /// <term>Token</term>
    /// <description>Jeton d'application.</description>
    /// </item>
    /// </list>
    /// </summary>
    [DataContract]
    public class Application
    {
        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string Token { get; set; }
    }
}
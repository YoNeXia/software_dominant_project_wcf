﻿using WCFMiddleware.Model;

namespace WCFMiddleware.Interfaces
{
    /// <summary>
    /// Interface exposant les méthodes d'authentification.
    /// </summary>
    public interface IAuthentication
    {
        string Authenticate(User user);

        string GetLoginByToken(string token);
    }
}
﻿using WCFMiddleware.Model;

namespace WCFMiddleware.Interfaces
{
    /// <summary>
    /// Interface exposant les méthode de déchiffrement.
    /// </summary>
    public interface IDecryptor
    {
        void DecryptFiles(Message message);

        File DecryptSecreteFile(File file);
    }
}
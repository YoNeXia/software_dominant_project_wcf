﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using File = WCFMiddleware.Model.File;

namespace WCFMiddleware.Utils
{
    /// <summary>
    /// Actions utilitaires.
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// Génère un document PDF contenant les résultats de l'analyse.
        /// </summary>
        /// <param name="result">Résultats.</param>
        public void GeneratePDF(string result)
        {
            FileStream fs = new FileStream("folder" + "\\" + "First PDF document.pdf", FileMode.Create);
            Document document = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();
            document.Add(new Paragraph(result));
            document.Close();
            writer.Close();
            fs.Close();
        }

        /// <summary>
        /// Envoie un mail de confirmation du déchiffrement.
        /// </summary>
        /// <param name="mailAdress">Adresse mail ou sera envoyé le mail.</param>
        public void SendMail(string mailAdress, File file)
        {
            try
            {
                var fromAddress = new MailAddress("projettyja@gmail.com");
                var toAddress = new MailAddress(mailAdress);
                const string fromPassword = "tyjaMail33";
                const string subject = "Information secrète";
                string body = String.Format("Incroyable ! L'information secrète a été trouvée !\nLa voici : {0}\nElle se situe dans le fichier {1} déchiffré avec la clé {2}.\n\nTexte en clair:\n{3}", file.SecreteInformation, file.Title, file.Key, file.ContentString);

                SmtpClient smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
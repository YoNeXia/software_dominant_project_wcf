﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using WCFMiddleware.Interfaces;
using WCFMiddleware.Model;
using File = WCFMiddleware.Model.File;

namespace WCFMiddleware.BU
{
    /// <summary>
    /// Classe de dechiffrage des fichiers.
    /// </summary>
    public class Decryptor : IDecryptor
    {
        private List<string> keys;
        private List<File> cryptedFiles;
        private Message incomingMessage;

        /// <summary>
        /// Lance le dechiffrage des fichiers reçus.
        /// </summary>
        /// <param name="message">Le message contenant la liste de fichiers.</param>
        public void DecryptFiles(Message message)
        {
            incomingMessage = message;
            cryptedFiles = incomingMessage.Files;
            this.GenerateKey(4);
            Parallel.ForEach(cryptedFiles, (file) =>
            {
                Parallel.ForEach(keys, (key) =>
                {
                    File decryptedFile = this.DecryptFile(file, key);
                    this.SendDecryptedFileToJEE(decryptedFile);
                });
            });
        }

        /// <summary>
        /// Lance le dechiffrement du fichier contenant l'information secrète
        /// </summary>
        /// <param name="file">Fichier à déchiffrer.</param>
        /// <returns>Fichier dechiffré.</returns>
        public File DecryptSecreteFile(File file)
        {
            return this.DecryptFile(file, file.Key);
        }

        /// <summary>
        /// Déchiffre un fichier avec une clé.
        /// </summary>
        /// <param name="file">Fichier à déchiffrer.</param>
        /// <param name="key">Clé pour déchiffrer le fichier.</param>
        /// <returns>Fichier déchiffré</returns>
        private File DecryptFile(File file, string key)
        {
            byte[] fileBytes = Encoding.ASCII.GetBytes(file.ContentString);
            byte[] keyBytes = Encoding.ASCII.GetBytes(key);
            char[] decoded = new char[file.ContentString.Length];
            for (int i = 0; i < file.ContentString.Length; i++)
            {
                byte cur = (byte)(fileBytes[i] ^ keyBytes[i % key.Length]);
                decoded[i] = Convert.ToChar(cur);
            }
            File result = new File()
            {
                Title = file.Title,
                ContentString = new string(decoded),
                Key = key,
                SecreteInformation = file.SecreteInformation
            };
            return result;
        }

        /// <summary>
        /// Génère l'ensemble des clés à tester pour bruteforcer le fichier.
        /// </summary>
        /// <param name="char_nmb">Taille de la clé à tester.</param>
        private void GenerateKey(int char_nmb)
        {
            keys = new List<string>();
            //Brute force : assume une clé de char_nmb caractères de l'alphabet français en majuscule.
            StringBuilder Mdp = new StringBuilder(new string('A', char_nmb));
            while (true)
            {
                string value = Mdp.ToString();
                keys.Add(value);
                if (value.All(item => item == 'Z')) //Put a Z here
                {
                    break;
                }
                for (int i = char_nmb - 1; i >= 0; --i)
                {
                    if (Mdp[i] != 'Z')
                    {
                        Mdp[i] = (char)(Mdp[i] + 1);
                        break;
                    }
                    else
                    {
                        Mdp[i] = 'A';
                    }
                }
            }
            //keys.Add("AWHI");
        }

        /// <summary>
        /// Envoi du fichier déchiffré à JEE.
        /// </summary>
        /// <param name="file">Le fichier déchiffré.</param>
        private void SendDecryptedFileToJEE(File file)
        {
            List<File> fileList = new List<File>() { file };
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://91.68.246.62:22128/restful/services/");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            Message message = new Message()
            {
                Operation = new Operation
                {
                    Statut = false,
                    Name = "CheckFile",
                    Infos = "Check operation for the tile sent to JEE platform",
                    Version = "1.0"
                },
                Application = new Application
                {
                    Version = "1.0",
                    Token = ConfigurationManager.AppSettings.Get("token")
                },
                User = new User
                {
                    Login = incomingMessage.User.Login,
                    Password = incomingMessage.User.Password,
                    Token = incomingMessage.User.Token
                },
                Files = fileList
            };

            using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(message);
                streamWriter.Write(json);
            }
            HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                string result = streamReader.ReadToEnd();
            }
        }
    }
}
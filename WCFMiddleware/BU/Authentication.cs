﻿using WCFDAO.DAO;
using WCFDAO.Interfaces;
using WCFMiddleware.Interfaces;
using WCFMiddleware.Model;

namespace WCFMiddleware.BU
{
    /// <summary>
    /// Classe d'authentification utilisateur.
    /// </summary>
    public class Authentication : IAuthentication
    {
        private IUserDAO instance = new UserDAO();

        /// <summary>
        /// Authentifie l'utilisateur.
        /// </summary>
        /// <param name="user">Objet utilisateur.</param>
        /// <returns>Le jeton utilisateur.</returns>
        public string Authenticate(User user)
        {
            return this.instance.GetUserToken(user.Login, user.Password);
        }

        /// <summary>
        /// Récupère les informations de connexion par le jeton utilisateur.
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Information de connexion</returns>
        public string GetLoginByToken(string token)
        {
            return this.instance.GetUserLoginByToken(token);
        }
    }
}
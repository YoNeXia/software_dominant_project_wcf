﻿using System;
using System.Data.Linq.Mapping;

namespace WCFDAO.Model
{
    [Table(Name = "dbo.user")]
    public class UserEntity
    {
        private int Id;

        /// <summary>
        /// Ensemble des getters et setters associés à l'entité User.
        /// <list type="bullet">
        /// <item>
        /// <term>id</term>
        /// <description>ID de l'utilisateur.</description>
        /// </item>
        /// <item>
        /// <term>Login</term>
        /// <description>Adresse mail de connexion de l'utilisateur.</description>
        /// </item>
        /// <item>
        /// <term>Password</term>
        /// <description>Mot de passe de l'utilisateur.</description>
        /// </item>
        /// <item>
        /// <term>Token</term>
        /// <description>Jeton de l'utilisateur.</description>
        /// </item>
        /// <item>
        /// <term>LastConnection</term>
        /// <description>Date de dernière connexion de l'utilisateur.</description>
        /// </item>
        /// </list>
        /// </summary>
        [Column(IsPrimaryKey = true, Storage = "Id")]
        public int id
        {
            get
            {
                return this.Id;
            }
            set
            {
                this.Id = value;
            }
        }

        private string Login;

        [Column(Storage = "Login")]
        public string login
        {
            get
            {
                return this.Login;
            }
            set
            {
                this.Login = value;
            }
        }

        private string Password;

        [Column(Storage = "Password")]
        public string password
        {
            get
            {
                return this.Password;
            }
            set
            {
                this.Password = value;
            }
        }

        private string Token;

        [Column(Storage = "Token")]
        public string token
        {
            get
            {
                return this.Token;
            }
            set
            {
                this.Token = value;
            }
        }

        private DateTime LastConnection;

        [Column(Storage = "LastConnection")]
        public DateTime last_connection
        {
            get
            {
                return this.LastConnection;
            }
            set
            {
                this.LastConnection = value;
            }
        }
    }
}
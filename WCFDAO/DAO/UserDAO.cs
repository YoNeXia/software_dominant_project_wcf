﻿using System.Data;
using System.Data.Linq;
using System.Linq;
using WCFDAO.Interfaces;
using WCFDAO.Model;

namespace WCFDAO.DAO
{
    /// <summary>
    /// <c>UserDAO<c> : Objet d'interface pour les données utilisateur dans la base de données.
    /// </summary>
    public class UserDAO : IUserDAO
    {
        private readonly DataContext dbContext = new DataContext("Server=tyja-sql-serv.database.windows.net,1433;Initial Catalog=UserDB;Persist Security " +
                "Info=False;User ID=adminDB;Password=tyjaDB33;MultipleActiveResultSets=False;Encrypt=True;" +
                "TrustServerCertificate=False;Connection Timeout=5000;");

        /// <summary>
        /// Effectue une requête en base pour retourner le jeton utilisateur.
        /// </summary>
        /// <param name="login">L'adresse mail utilisée pour se connecter.</param>
        /// <param name="password">Le mot de passe utilisé pour se connecter.</param>
        /// <returns>Jeton Utilisateur</returns>
        public string GetUserToken(string login, string password)
        {
            Table<UserEntity> Users = dbContext.GetTable<UserEntity>();
            IQueryable<UserEntity> userQuery = from user in Users where user.login == login && user.password == password select user;
            return userQuery.Select(user => user.token).FirstOrDefault();
        }

        /// <summary>
        /// Effectue une requête en base pour retourner le login d'authentification utilisateur.
        /// </summary>
        /// <param name="token"></param>
        /// <returns>L'adresse mail de l'utilisateur</returns>
        public string GetUserLoginByToken(string token)
        {
            Table<UserEntity> Users = dbContext.GetTable<UserEntity>();
            IQueryable<UserEntity> userQuery = from user in Users where user.token == token select user;
            return userQuery.Select(user => user.login).FirstOrDefault();
        }
    }
}
﻿namespace WCFDAO.Interfaces
{
    /// <summary>
    /// Interface exposant les méthodes disponibles pour le DAO User.
    /// </summary>
    public interface IUserDAO
    {
        string GetUserToken(string login, string password);

        string GetUserLoginByToken(string token);
    }
}